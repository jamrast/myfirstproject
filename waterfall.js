const async = require("async");
var step1 = function(callback) {
    console.log('step1 ');
    callback(null, "1");
}

var step2 = function(arg1,callback) {
    console.log("step2");
    callback(null,"2")
}


var finalStep = function(err, result) {
    if(err) {
        console.log("error")
    } else {
        console.log(result);
    }
}

async.waterfall([
    step1,
    step2
], finalStep);