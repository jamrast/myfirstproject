var express = require('express');
var mysql = require('mysql');
var router = express.Router();

const pool = mysql.createPool({
    host: 'localhost',
    user: 'usr',
    password: 'usr',
    database: 'test_db',
    connectionLimit: 10,
    multipleStatements: true,
});

router.get('/', function(req, res){
   // res.end("get user");
  // let id = req.params.id;
   let user = req.query;

  let sql = `
  select id, code, name, email
  from user
  where code like('%${user.code}%') 
  and   name like('%${user.name}%') 
`;

  pool.getConnection( function(err, connection) {
      if(err) {
          res.end("error....");
      }else {
          connection.query(sql, function(error, result){
              connection.release();  // release connection, otherwise connection will full
              if(error) {
                  console.log(sql);
                  res.end("sql error = " + error);
              }else {
                  res.json(result);
              }
          });
      }
  });

});

router.post('/', function(req, res){
    let user = req.body;
    let sql = `
        insert into user( code, name, pwd, email)
        values('${user.code}', '${user.name}', '${user.pwd}', '${user.email}' )
    `;
    pool.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                connection.release();  // release connection, otherwise connection will full
                if(error) {
                    res.end("sql error = " + error);
                }else {
                    res.end("insert success");
                }
            });
        }
    });
    
});

router.put('/:id', function(req, res){
   // res.end("update user");
    let id = req.params.id;
    let user = req.body;

   let sql = `
   update user
    set code = '${user.code}',
        name = '${user.name}'
   where id = ${id}
`;

   pool.getConnection( function(err, connection) {
       if(err) {
           res.end("error....");
       }else {
           connection.query(sql, function(error, result){
               connection.release();  // release connection, otherwise connection will full
               if(error) {
                   console.log(sql);
                   res.end("sql error = " + error);
               }else {
                   res.end("update success");
               }
           });
       }
   });
});

router.delete('/:code', function(req, res){
    let code = req.params.code;
    let sql = `
    delete from user
    where code = ${code}
`;

    pool.getConnection( function(err, connection) {
        if(err) {
            res.end("error....");
        }else {
            connection.query(sql, function(error, result){
                connection.release();  // release connection, otherwise connection will full
                if(error) {
                    res.end("sql error = " + error);
                }else {
                    res.end("delete success");
                }
            });
        }
    });

  //  res.end("delete user");
});

module.exports = router;
// https://gitlab.com/sommai.k/simple2