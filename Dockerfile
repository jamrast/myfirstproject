# comment
FROM node:14-alpine
WORKDIR /app
COPY . /app
RUN npm i 
EXPOSE 8000
CMD ["node", "app.js"]

# docker build -t first .
#
# docker image ls
# docker run --name first -p 8000:8000 -d first

# start DB
# get from link https://gitlab.com/jamrast/myfirstproject/container_registry
# docker login registry.gitlab.com
# docker tag first registry.gitlab.com/jamrast/myfirstproject
# docker build -t registry.gitlab.com/jamrast/myfirstproject .
# docker push registry.gitlab.com/jamrast/myfirstproject
# send the link registry.gitlab.com/jamrast/myfirstproject to other people then they can use this image in docker

